use chrono::{DateTime, Utc};
use chrono::serde::ts_seconds::deserialize as from_ts;

use std::collections::HashMap;
use std::net::IpAddr;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Container {
    #[serde(rename = "Id")]
    pub id: String,
    #[serde(rename = "Names")]
    pub names: Vec<String>,
    #[serde(rename = "Image")]
    pub image: String,
    #[serde(rename = "ImageID")]
    pub image_id: String,
    #[serde(rename = "Command")]
    pub command: String,
    #[serde(rename = "Created", deserialize_with = "from_ts")]
    pub created: DateTime<Utc>,
    #[serde(rename = "Ports")]
    pub ports: Vec<Port>,
    #[serde(rename = "Labels", default)]
    pub labels: HashMap<String, String>,
    #[serde(rename = "State")]
    pub state: String,
    #[serde(rename = "Status")]
    pub status: String,
    #[serde(rename = "HostConfig")]
    pub host_config: HostConfig,
    #[serde(rename = "NetworkSettings")]
    pub network_settings: NetworkSettings,
    #[serde(rename = "Mounts")]
    pub mounts: Vec<Mount>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Port {
    #[serde(rename = "IP")]
    pub ip: IpAddr,
    #[serde(rename = "PrivatePort")]
    pub private_port: i64,
    #[serde(rename = "PublicPort")]
    pub public_port: i64,
    #[serde(rename = "Type")]
    pub type_field: String,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct HostConfig {
    #[serde(rename = "NetworkMode")]
    pub network_mode: String,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct NetworkSettings {
    #[serde(rename = "Networks", default)]
    pub networks: HashMap<String, Network>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Network {
    #[serde(rename = "IPAMConfig")]
    pub ipamconfig: ::serde_json::Value,
    #[serde(rename = "Links")]
    pub links: ::serde_json::Value,
    #[serde(rename = "Aliases")]
    pub aliases: ::serde_json::Value,
    #[serde(rename = "NetworkID")]
    pub network_id: String,
    #[serde(rename = "EndpointID")]
    pub endpoint_id: String,
    #[serde(rename = "Gateway")]
    pub gateway: Option<IpAddr>,
    #[serde(rename = "IPAddress")]
    pub ipaddress: Option<IpAddr>,
    #[serde(rename = "IPPrefixLen")]
    pub ipprefix_len: i64,
    #[serde(rename = "IPv6Gateway")]
    pub ipv6_gateway: String,
    #[serde(rename = "GlobalIPv6Address")]
    pub global_ipv6_address: String,
    #[serde(rename = "GlobalIPv6PrefixLen")]
    pub global_ipv6_prefix_len: i64,
    #[serde(rename = "MacAddress")]
    pub mac_address: String,
    #[serde(rename = "DriverOpts")]
    pub driver_opts: ::serde_json::Value,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Mount {
    #[serde(rename = "Type")]
    pub type_field: String,
    #[serde(rename = "Name")]
    pub name: String,
    #[serde(rename = "Source")]
    pub source: String,
    #[serde(rename = "Destination")]
    pub destination: String,
    #[serde(rename = "Driver")]
    pub driver: String,
    #[serde(rename = "Mode")]
    pub mode: String,
    #[serde(rename = "RW")]
    pub rw: bool,
    #[serde(rename = "Propagation")]
    pub propagation: String,
}
