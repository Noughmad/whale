use hyper::{Uri, Method, Body};
use hyper::client::{Client, FutureResponse, HttpConnector, Request};
use tokio_core::reactor::Handle;

use std::path::PathBuf;

#[cfg(unix)]
use hyperlocal::{self, UnixConnector};

use failure::Error;

use serde_json;
use serde::de::DeserializeOwned;
use serde::ser::Serialize;

use futures::{Future, Stream};

use container::Container;

pub enum Docker {
    #[cfg(unix)]
    LocalSocket {
        socket_path: PathBuf,
        client: Client<UnixConnector>,
        version: String,
    },
    Http {
        uri: Uri,
        client: Client<HttpConnector>,
        version: String,
    }
}

fn uri_with_path(base: &Uri, path: &str) -> Uri {
    let mut s = base.to_string();
    s.push_str(path);
    s.parse().unwrap()
}

impl Docker {
    pub fn new(uri: Uri, handle: Handle) -> Result<Docker, Error> {
        match uri.scheme() {
            Some("http") => Ok(Docker::Http {
                uri: uri.clone(),
                client: Client::configure().build(&handle),
                version: "1.24".to_string(),
            }),
            #[cfg(unix)]
            Some("unix") => Ok(Docker::LocalSocket {
                socket_path: PathBuf::from(uri.path()),
                client: Client::configure()
                    .connector(UnixConnector::new(handle.clone()))
                    .build(&handle),
                version: "1.24".to_string(),
            }),
            s => bail!("Unexpected URI scheme: {:?}", s),
        }
    }

    pub fn get(&self, path: &str) -> FutureResponse {
        match self {
            Docker::Http {uri, client, version} => {
                client.get(uri_with_path(&uri, &format!("/v{}/{}/json", version, path)))
            },
            #[cfg(unix)]
            Docker::LocalSocket {socket_path, client, version} => {
                let path = format!("/v{}/{}/json", version, path);
                let url = hyperlocal::Uri::new(socket_path, &path);
                client.get(url.into())
            },
        }
    }

    pub fn post(&self, path: &str, body: Body) -> FutureResponse {
        match self {
            Docker::Http {uri, client, version} => {
                let mut request = Request::new(Method::Post, uri_with_path(&uri, &format!("/v{}/{}/json", version, path)));
                request.set_body(body);
                client.request(request)
            },
            #[cfg(unix)]
            Docker::LocalSocket {socket_path, client, version} => {
                let path = format!("/v{}/{}/json", version, path);
                let mut request = Request::new(Method::Post, hyperlocal::Uri::new(socket_path, &path).into());
                request.set_body(body);
                client.request(request)
            },
        }
    }

    pub fn get_json<T: DeserializeOwned>(&self, path: &str) -> impl Future<Item=T, Error=Error> {
        self.get(path)
            .and_then(|res| res.body().concat2())
            .from_err::<Error>()
            .and_then(|chunk| serde_json::from_slice(&chunk).map_err(From::from))
    }

    pub fn post_json<B: Serialize, T: DeserializeOwned>(&self, path: &str, body: &B) -> impl Future<Item=T, Error=Error> {
        self.post(path, serde_json::to_vec(body).unwrap().into())
            .and_then(|res| res.body().concat2())
            .from_err::<Error>()
            .and_then(|chunk| serde_json::from_slice(&chunk).map_err(From::from))
    }

    pub fn list_containers(&self) -> impl Future<Item=Vec<Container>, Error=Error> {
        self.get_json("containers")
    }

    pub fn inspect_container(&self, name: &str) -> impl Future<Item=Container, Error=Error> {
        self.get_json(&format!("container/{}", name))
    }
}
