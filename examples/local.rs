extern crate whale;
extern crate tokio_core;
extern crate hyper;
extern crate futures;
extern crate failure;

use std::str::FromStr;

use futures::future::Future;
use tokio_core::reactor::Core;

use failure::Error;

fn run() -> Result<(), Error> {
    let mut core = Core::new()?;
    let handle = core.handle();

    let docker = whale::Docker::new(hyper::Uri::from_str("unix://var/run/docker.sock")?, handle).unwrap();

    let work = docker.list_containers();

    let containers = core.run(work)?;
    println!("{:#?}", containers);

    Ok(())
}

fn main() {
    run().unwrap();
}
